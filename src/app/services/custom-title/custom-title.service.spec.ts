import { TestBed } from '@angular/core/testing';

import { CustomTitleService } from './custom-title.service';

describe('CustomTitleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomTitleService = TestBed.get(CustomTitleService);
    expect(service).toBeTruthy();
  });
});
