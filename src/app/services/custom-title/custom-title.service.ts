import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class CustomTitleService {

  private _prefix: string;


  constructor(private title: Title) {
    this._prefix = 'ST: ';
  }

  public setTitle(newTitle: string): void {
    this.title.setTitle(this._prefix + newTitle);
  }

  public getTitle(): string {
    return this.title.getTitle();
  }
}
