import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private static _nameCategory: string = 'adm';


  constructor() { }

  public static getNameCategory(): string {
    return this._nameCategory;
  }
}
