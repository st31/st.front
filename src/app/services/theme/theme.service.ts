import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private _color: string;
  private _nameFieldTheme: string;
  private static readonly nameFieldTheme: string = 'theme';


  constructor(private alertController: AlertController) {
    this._nameFieldTheme = 'theme';
  }

  public static getCurrentTheme(): string {
    return localStorage.getItem(this.nameFieldTheme);
  }

  public static setCurrentTheme(nameTheme: string): void {
    localStorage.setItem(this.nameFieldTheme, nameTheme);
  }

  public async presentAlertChangeTheme() {
    const currentTheme: string = localStorage.getItem(this._nameFieldTheme);
    const alert = await this.alertController.create({
      header: 'Theme',
      inputs: [
        {
          name: 'dark-theme',
          type: 'radio',
          label: 'Dark',
          value: 'dark',
          checked: currentTheme === 'dark'
        },
        {
          name: 'purple-theme',
          type: 'radio',
          label: 'Blueberry',
          value: 'tertiary',
          checked: currentTheme === 'tertiary'
        },
        {
          name: 'light-theme',
          type: 'radio',
          label: 'Bright future',
          value: 'light',
          checked: currentTheme === 'light'
        },
        {
          name: 'medium-theme',
          type: 'radio',
          label: 'Ashes',
          value: 'medium',
          checked: currentTheme === 'medium'
        },
        {
          name: 'danger-theme',
          type: 'radio',
          label: 'Red lipstick',
          value: 'danger',
          checked: currentTheme === 'danger'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: async (value) => {
            if (currentTheme === value) {
              return;
            }

            this._color = value;
            localStorage.setItem(this._nameFieldTheme, value);

            this.changeColorAddressLine();
            await this.changeDOMIonItems(currentTheme);
            await this.changeDOM(currentTheme);
          }
        }
      ]
    });

    this.changeColorsOfElement(currentTheme);


    await alert.present();
  }

  public changeColorAddressLine(newColor: string = null): void {
    let hexColor: string = '';

    if (newColor === null) {
      newColor = this._color;
    }

    switch (newColor) {
      case 'dark':
        hexColor = '#222428';
        break;
      case 'tertiary':
        hexColor = '#7044ff';
        break;
      case 'light':
        hexColor = '#f4f5f8';
        break;
      case 'medium':
        hexColor = '#989aa2';
        break;
      case 'danger':
        hexColor = '#f04141';
        break;
    }

    document.querySelector("meta[name='theme-color']").setAttribute('content', hexColor);
  }

  private changeColorsOfElement(currentTheme: string) {
    if (currentTheme === 'dark') {
      this.setAttributesColorsOfElements('background:#212121', 'color:#ffffff', 'color:#ffffff');
    }
    else if (currentTheme === 'tertiary') {
      this.setAttributesColorsOfElements('background:#341f76', 'color:#ffffff', 'color:#ffffff');
    }
    else if (currentTheme === 'light') {
      this.setAttributesColorsOfElements('background:#e6e6e6', 'color:#000000', 'color:#000000');
    }
    else if (currentTheme === 'medium') {
      this.setAttributesColorsOfElements('background:#4d4e54', 'color:#dadada', 'color:#dadada');
    }
    else if (currentTheme === 'danger') {
      this.setAttributesColorsOfElements('background:#a52424', 'color:#ffffff', 'color:#ffffff');
    }
  }

  private setAttributesColorsOfElements(
    backgroundAlertWrapper: string,
    colorAlertTitle: string,
    colorRadioLabel: string,
  ) {

    document.getElementsByClassName('alert-wrapper')[0].setAttribute('style', backgroundAlertWrapper);
    document.getElementsByClassName('alert-title')[0].setAttribute('style', colorAlertTitle);
    document.getElementsByClassName('alert-button')[0].setAttribute('style', colorAlertTitle);

    let alertRadioLabels = document.getElementsByClassName('alert-radio-label');
    for (let index = 0; index < alertRadioLabels.length; index++) {
      alertRadioLabels[index].setAttribute('style', colorRadioLabel);
    }
  }

  private async changeDOM(oldColor: string): Promise<void> {
    let allDOM: HTMLCollectionOf<Element> = document.getElementsByTagName('*');
    for (let index = 0; index < allDOM.length; index++) {
      if (allDOM[index].classList.contains(`ion-color-${oldColor}`) && allDOM[index].tagName.toLowerCase() !== 'ion-item') {
        allDOM[index].classList.remove(`ion-color-${oldColor}`);
        allDOM[index].classList.add(`ion-color-${this._color}`);
      }
    }
  }

  private async changeDOMIonItems(oldColor: string): Promise<void> {
    let ionItems: HTMLCollectionOf<HTMLIonItemElement> = document.getElementsByTagName('ion-item');
    for (let index = 0; index < ionItems.length; index++) {
      setTimeout(() => {
        ionItems[index].classList.remove(`ion-color-${oldColor}`);
        ionItems[index].classList.add(`ion-color-${this._color}`);
      }, 50 * index);
    }
  }
}
