import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Api } from '../../interfaces/api/api.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _apiUrl: string;

  private httpOptions = {
    withCredentials: true,
    crossdomain: true,
    headers: new HttpHeaders({
      "Access-Control-Allow-Origin": "http://st.web",
      "Access-Control-Allow-Credentials": "true",
      "Content-Type": "application/json",
      "dataType": "json",
      "Access-Control-Allow-Headers": "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers",
    }),
  };


  constructor(private _http: HttpClient) {
    this._apiUrl = 'http://st.web/api/';
  }

  public registration() {

  }

  public async auth(login: string, password: string): Promise<Api> {
    let url: string = this._apiUrl + 'security/access/auth';

    let response = await this._http.post(url, {
      "login": login,
      "password": password
    }, this.httpOptions)
      .toPromise()
      .then((value) => {
        return value;
      })
      .catch((e) => {
        return e;
      });

    return response;
  }

  public roleAdd() {

  }

  public roleEdit() {

  }

  public roleRemove() {

  }

  public permissionAdd() {

  }

  public permissionRemove() {

  }
}
