import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CabinetService {

  private static _nameCategory: string = 'cab';


  constructor() { }

  public static getNameCategory(): string {
    return this._nameCategory;
  }
}
