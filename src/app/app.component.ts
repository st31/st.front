import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ThemeService } from './services/theme/theme.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public pages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
      level: 0
    },
    {
      title: 'Login',
      url: '/home/login',
      icon: 'log-in',
      level: 0
    },
    {
      title: 'Logout',
      url: '/',
      icon: 'log-out',
      level: 0
    },
    {
      title: 'Feedback',
      url: '/home/feedback',
      icon: 'cafe',
      level: 0
    },
    {
      title: 'Didactic materials',
      url: '/didactic-materials',
      icon: 'book',
      level: 0
    },
    {
      title: 'Details',
      url: '/didactic-materials/details',
      icon: 'book',
      level: 1
    },
    {
      title: 'Testing',
      url: '/testing',
      icon: 'finger-print',
      level: 0
    },
    {
      title: 'Individual-test',
      url: '/testing/individual-test',
      icon: 'body',
      level: 1
    },
    {
      title: 'Admin',
      url: '/admin',
      icon: 'code-working',
      level: 0
    },
    {
      title: 'Statistics',
      url: '/admin/statistics',
      icon: 'pie',
      level: 1
    },
    {
      title: 'Testing',
      url: '/admin/testing',
      icon: 'help',
      level: 1
    },
    {
      title: 'Didactic materials',
      url: '/admin/didactic-materials',
      icon: 'paper',
      level: 1
    },
    {
      title: 'Registration',
      url: '/admin/registration',
      icon: 'person-add',
      level: 1
    },
    {
      title: 'Roles',
      url: '/admin/roles',
      icon: 'switch',
      level: 1
    },
    {
      title: 'Users',
      url: '/admin/users',
      icon: 'people',
      level: 1
    },
    {
      title: 'Cabinet',
      url: '/cabinet',
      icon: 'headset',
      level: 0
    },
    {
      title: 'Settings',
      url: '/cabinet/settings',
      icon: 'settings',
      level: 1
    },
    {
      title: 'About app',
      url: '/home/about-app',
      icon: 'information',
      level: 0
    }
  ];

  private _color: string;


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private themeService: ThemeService,
  ) {

    this.initializeApp();

    this._installationTheme();
  }

  get currentYear(): string {
    let result: string = new Date().getFullYear().toString();
    return result;
  }

  get color(): string {
    return this._color;
  }

  public changeTheme(): void {
    this.themeService.presentAlertChangeTheme();
  }

  private _installationTheme(): void {
    let currentTheme: string = ThemeService.getCurrentTheme();
    if (currentTheme === undefined || currentTheme === null || currentTheme.length === 0) {
      ThemeService.setCurrentTheme('dark');
    }

    this._color = ThemeService.getCurrentTheme();
    this.themeService.changeColorAddressLine(this._color);
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
