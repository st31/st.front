import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { ErrorPage } from './pages/not_categorized/error/error.page';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/not_categorized/not-categorized.module').then(m => m.NotCategorizedModule),
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminPageModule),
  },
  {
    path: 'cabinet',
    loadChildren: () => import('./pages/cabinet/cabinet.module').then(m => m.CabinetPageModule),
  },
  {
    path: 'didactic-materials',
    loadChildren: () => import('./pages/didactic-materials/didactic-materials.module').then(m => m.DidacticMaterialsPageModule),
  },
  {
    path: 'testing',
    loadChildren: () => import('./pages/testing/testing.module').then(m => m.TestingPageModule),
  },
  {
    path: '**',
    component: ErrorPage
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
