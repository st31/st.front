import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { IndividualTestPage } from './individual-test/individual-test.page';
import { PassTestPage } from './pass-test/pass-test.page';
import { TestingRoutingModule } from './testing-routing.module';
import { HomePage } from './home/home.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestingRoutingModule,
  ],
  declarations: [
    HomePage,
    IndividualTestPage,
    PassTestPage
  ]
})
export class TestingPageModule { }
