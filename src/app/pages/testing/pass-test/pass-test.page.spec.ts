import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassTestPage } from './pass-test.page';

describe('PassTestPage', () => {
  let component: PassTestPage;
  let fixture: ComponentFixture<PassTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassTestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
