import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pass-test',
  templateUrl: './pass-test.page.html',
  styleUrls: ['./pass-test.page.scss'],
})
export class PassTestPage implements OnInit {

  private _title: string;


  constructor() {
    this._title = 'Pass the test';
  }

  get title(): string {
    return this._title;
  }

  ngOnInit() {
  }

}
