import { Component, OnInit } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-individual-test',
  templateUrl: './individual-test.page.html',
  styleUrls: ['./individual-test.page.scss'],
})
export class IndividualTestPage implements OnInit {

  private _title: string;


  constructor(private titleService: CustomTitleService) {
    this._title = 'individual test';

    this.titleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  ngOnInit() {
  }

}
