import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomePage } from './home/home.page';
import { PassTestPage } from './pass-test/pass-test.page';
import { IndividualTestPage } from './individual-test/individual-test.page';


const ROUTES: Routes = [
    {
        path: '',
        component: HomePage
    },
    {
        path: 'pass-test',
        component: PassTestPage
    },
    {
        path: 'individual-test',
        children: [
            {
                path: '',
                component: IndividualTestPage
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})
export class TestingRoutingModule { }
