import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home/home.page';
import { DidacticMaterialsPage } from './didactic-materials/didactic-materials.page';
import { RegistrationPage } from './registration/registration.page';
import { RolesPage } from './roles/roles.page';
import { StatisticsPage } from './statistics/statistics.page';
import { TestingPage } from './testing/testing.page';
import { UsersPage } from './users/users.page';



const ROUTES: Routes = [
    {
        path: '',
        component: HomePage,
    },
    {
        path: 'didactic-materials',
        component: DidacticMaterialsPage,
    },
    {
        path: 'registration',
        component: RegistrationPage,
    },
    {
        path: 'roles',
        component: RolesPage,
    },
    {
        path: 'statistics',
        component: StatisticsPage,
    },
    {
        path: 'testing',
        component: TestingPage,
    },
    {
        path: 'users',
        component: UsersPage,
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class AdminRoutingModule { }

