import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home/home.page';
import { AdminRoutingModule } from './admin-routing.module';
import { DidacticMaterialsPage } from './didactic-materials/didactic-materials.page';
import { RegistrationPage } from './registration/registration.page';
import { RolesPage } from './roles/roles.page';
import { StatisticsPage } from './statistics/statistics.page';
import { TestingPage } from './testing/testing.page';
import { UsersPage } from './users/users.page';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminRoutingModule,
  ],
  declarations: [
    HomePage,
    DidacticMaterialsPage,
    RegistrationPage,
    RolesPage,
    StatisticsPage,
    TestingPage,
    UsersPage
  ]
})
export class AdminPageModule { }
