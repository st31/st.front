import { Component, OnInit } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { AdminService } from '../../../services/admin/admin.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-didactic-materials',
  templateUrl: './didactic-materials.page.html',
  styleUrls: ['./didactic-materials.page.scss'],
})
export class DidacticMaterialsPage implements OnInit {

  private _title: string;


  constructor(private customTitleService: CustomTitleService) {
    this._title = 'didactic materials';

    this.customTitleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  get nameCategory(): string {
    return AdminService.getNameCategory();
  }

  ngOnInit() {
  }

}
