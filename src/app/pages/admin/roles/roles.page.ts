import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/admin/admin.service';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.page.html',
  styleUrls: ['./roles.page.scss'],
})
export class RolesPage implements OnInit {

  private _title: string;


  constructor(private customTitleService: CustomTitleService) {
    this._title = 'system of the roles';

    this.customTitleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  get nameCategory(): string {
    return AdminService.getNameCategory();
  }

  ngOnInit() {
  }

}
