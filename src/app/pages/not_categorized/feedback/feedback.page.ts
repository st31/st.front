import { Component, OnInit } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

  private _title: string;

  public feedbackFormGroup: FormGroup;
  public firstname: AbstractControl;
  public lastname: AbstractControl;


  constructor(private titleService: CustomTitleService, public formbuilder: FormBuilder) {

    this.feedbackFormGroup = formbuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', [Validators.required, Validators.maxLength(5)]]
    });

    this.firstname = this.feedbackFormGroup.controls['firstname'];
    this.lastname = this.feedbackFormGroup.controls['lastname'];


    this._title = 'feedback';

    this.titleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title() {
    return this._title;
  }

  ngOnInit() {
  }

}
