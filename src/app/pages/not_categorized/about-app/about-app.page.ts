import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';


declare function require(name: string);

@Component({
  selector: 'app-about-app',
  templateUrl: './about-app.page.html',
  styleUrls: ['./about-app.page.scss'],
})
export class AboutAppPage implements OnInit {

  private _title: string;
  private _packageJson: JSON;


  constructor(private titleService: CustomTitleService) {
    this._title = 'about app';
    this.titleService.setTitle(this._title);

    this._packageJson = require('../../../../../package.json');
  }

  get title(): string {
    return this._title;
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get appNumberVersion(): string {
    return this._packageJson['version']
  }

  get appNumberBuild(): string {
    return this._packageJson['build'];
  }

  get background(): string {
    return '../../../../assets/background/about-app.jpg';
  }

  ngOnInit() {
  }

}
