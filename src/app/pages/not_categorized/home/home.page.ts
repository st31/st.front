import { Component } from '@angular/core';
import { Names } from '../../../classes/names/names';
import { CustomTitleService } from '../../../services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private _title: string;
  private _nameUser: string;


  constructor(private names: Names, private serviceTitle: CustomTitleService) {
    this._title = this.names.nameProject;

    this.serviceTitle.setTitle('home');
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title() {
    return this._title;
  }

  get nameUser() {
    return this._nameUser;
  }
}
