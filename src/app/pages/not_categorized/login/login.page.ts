import { Component, OnInit, OnDestroy } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { ApiService } from 'src/app/services/api/api.service';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Api } from 'src/app/interfaces/api/api.interface';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  public loginFormGroup: FormGroup;
  public login: AbstractControl;
  public password: AbstractControl;

  private _title: string;
  private _subscriptionLogin: Subscription;
  private _subscriptionPassword: Subscription;
  private _errorLoginMessage: string;
  private _errorPasswordMessage: string;


  constructor(
    private titleService: CustomTitleService,
    private _apiService: ApiService,
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _alertController: AlertController,
  ) {

    this._title = 'log in';
    this.titleService.setTitle(this._title);

    this.loginFormGroup = _formbuilder.group({
      login: ['', [Validators.required, this.noWhitespaceValidator]],
      password: ['', [Validators.required, this.noWhitespaceValidator]]
    });

    this.login = this.loginFormGroup.controls['login'];
    this.password = this.loginFormGroup.controls['password'];

    this.handlerChangesForm();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  async logForm(): Promise<void> {
    if (!this.loginFormGroup.valid) {
      return;
    }

    const login: string = this.login.value.trim();
    const password: string = this.password.value.trim();

    let resultAuth: Api = await this._apiService.auth(login, password)
      .then((value) => {
        return value;
      });

    if (resultAuth.status === 200) {
      if (resultAuth.data.result.code != null && resultAuth.data.result.code === 2) {
        this._router.navigateByUrl('/home');
        this.loginFormGroup.reset();
      }
      else if (resultAuth.data.result.code != null && resultAuth.data.result.code === 1) {
        this.presentAlert('Log In', (resultAuth.data.result.message));
        this.loginFormGroup.reset();
      }
    }
    else if (!navigator.onLine) {
      this.presentAlert('Error', 'No Internet connection');
      console.error(resultAuth);
    }
    else {
      this.presentAlert('Error', 'Error occurred please try again later');
      console.error(resultAuth);
      this.loginFormGroup.reset();
    }
  }

  private handlerChangesForm(): void {
    if (this.login.value.length === 0) {
      this._errorLoginMessage = 'Login not have empty';
    }
    this._subscriptionLogin = this.login.valueChanges
      .subscribe((value) => {
        if (!value || value.length === 0) {
          this._errorLoginMessage = 'Login not have empty';
        }
      });

    if (this.password.value.length === 0) {
      this._errorPasswordMessage = 'Password not have empty';
    }
    this._subscriptionPassword = this.password.valueChanges
      .subscribe((value) => {
        if (!value || value.length === 0) {
          this._errorPasswordMessage = 'Password not have empty';
        }
      });
  }

  private async presentAlert(header: string, message: string) {
    const alert = await this._alertController.create({
      header: header.trim(),
      message: message.trim(),
      buttons: ['OK']
    });

    await alert.present();
  }

  get colorSubmit(): string {
    switch (this.color) {
      case 'dark':
        return 'light';
      case 'tertiary':
        return 'light';
      case 'medium':
        return 'dark';
      case 'light':
        return 'dark';
      case 'danger':
        return 'success';
    }
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title() {
    return this._title;
  }

  get errorLogin(): string {
    return this._errorLoginMessage;
  }

  get errorPassword(): string {
    return this._errorPasswordMessage;
  }


  ngOnInit() {
  }

  ngOnDestroy(): void {
    this._subscriptionLogin.unsubscribe();
    this._subscriptionPassword.unsubscribe();
  }
}
