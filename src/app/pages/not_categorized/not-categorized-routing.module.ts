import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './home/home.page';
import { AboutAppPage } from './about-app/about-app.page';
import { FeedbackPage } from './feedback/feedback.page';
import { LoginPage } from './login/login.page';


const ROUTES: Routes = [
    {
        path: '',
        component: HomePage,
    },
    {
        path: 'about-app',
        component: AboutAppPage,
    },
    {
        path: 'feedback',
        component: FeedbackPage,
    },
    {
        path: 'login',
        component: LoginPage,
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class NotCategorizedRoutingModule { }

