import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomePage } from './home/home.page';
import { LoginPage } from './login/login.page';
import { FeedbackPage } from './feedback/feedback.page';
import { AboutAppPage } from './about-app/about-app.page';
import { NotCategorizedRoutingModule } from './not-categorized-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    NotCategorizedRoutingModule,
  ],
  declarations: [
    HomePage,
    AboutAppPage,
    FeedbackPage,
    LoginPage
  ]
})
export class NotCategorizedModule { }
