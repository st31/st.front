import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  @ViewChild(IonInfiniteScroll, { static: true }) infiniteScroll: IonInfiniteScroll;

  private _title: string;
  private _numberColums: number;

  public items: Array<undefined> = [
    undefined, undefined, undefined, undefined, undefined
  ];


  constructor(private titleService: CustomTitleService) {
    this._title = 'Details'
    this._numberColums = 5;

    this.changeNumberColumn(window.innerWidth);
    this.titleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  get numberColums(): Array<null> {
    let result: Array<null> = new Array(this._numberColums).fill(null);

    return result;
  }

  onResize(event) {
    let innerWidth: number = event.target.innerWidth;
    this.changeNumberColumn(innerWidth);
  }

  private changeNumberColumn(value: number): void {
    if (innerWidth <= 1210) {
      this._numberColums = 3;
    }
    else {
      this._numberColums = 5;
    }
  }


  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.items.push(undefined);

      if (this.items.length > 10) {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
      }
    }, 1000);
  }

  ngOnInit() {
  }

}
