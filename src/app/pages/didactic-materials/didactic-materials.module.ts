import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home/home.page';
import { DidacticMaterialsRoutingModule } from './didactic-materials-routing.module';
import { DetailsPage } from './details/details.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DidacticMaterialsRoutingModule,
  ],
  declarations: [
    HomePage,
    DetailsPage,
  ]
})

export class DidacticMaterialsPageModule { }
