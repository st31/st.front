import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { ThemeService } from 'src/app/services/theme/theme.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-didactic-materials',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  private _title: string;
  private _listingDiscipline: Array<Discipline>;


  constructor(private titleService: CustomTitleService, private router: Router) {
    this._title = 'didactic materials'
    this.titleService.setTitle(this._title);

    this._listingDiscipline = [
      {
        nameDiscipline: 'English',
        nameTeacher: 'Elena Vladimirovna Kuznetsova',
        icon: "easel"
      },
      {
        nameDiscipline: 'Mathematic',
        nameTeacher: 'Elena Vladimirovna Kuznetsova',
        icon: "easel"
      },
      {
        nameDiscipline: 'Physic',
        nameTeacher: 'Elena Vladimirovna Kuznetsova',
        icon: "easel"
      },
      {
        nameDiscipline: 'Chemie',
        nameTeacher: 'Elena Vladimirovna Kuznetsova',
        icon: "easel"
      }
    ];
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  get listingDiscipline(): Array<Discipline> {
    return this._listingDiscipline;
  }

  public goToDiscipline(item: number): void {
    console.log('Go to page...', item);
    this.router.navigateByUrl('didactic-materials/details');
  }



  ngOnInit() {
  }

}

interface Discipline {
  nameDiscipline: string;
  nameTeacher: string;
  icon: string;
}