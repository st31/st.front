import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home/home.page';
import { DetailsPage } from './details/details.page';


const ROUTES: Routes = [
    {
        path: '',
        component: HomePage,
    },
    {
        path: 'details',
        component: DetailsPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class DidacticMaterialsRoutingModule { }

