import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './home/home.page';
import { SettingsPage } from './settings/settings.page';


const ROUTES: Routes = [
    {
        path: '',
        component: HomePage,
    },
    {
        path: 'settings',
        component: SettingsPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class CabinetRoutingModule { }

