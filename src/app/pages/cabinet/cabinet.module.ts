import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home/home.page';
import { CabinetRoutingModule } from './cabinet-routing.module';
import { SettingsPage } from './settings/settings.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CabinetRoutingModule
  ],
  declarations: [
    HomePage,
    SettingsPage,
  ]
})
export class CabinetPageModule { }
