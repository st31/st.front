import { Component, OnInit } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { CabinetService } from '../../../services/cabinet/cabinet.service';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  private _title: string;


  constructor(private titleService: CustomTitleService) {
    this._title = 'settings';

    this.titleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  get nameCategory(): string {
    return CabinetService.getNameCategory();
  }

  ngOnInit() {
  }

}
