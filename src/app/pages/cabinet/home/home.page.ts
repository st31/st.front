import { Component, OnInit } from '@angular/core';
import { CustomTitleService } from 'src/app/services/custom-title/custom-title.service';
import { CabinetService } from '../../../services/cabinet/cabinet.service';
import { ThemeService } from 'src/app/services/theme/theme.service';


@Component({
  selector: 'app-cabinet',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  private _title: string;

  constructor(private titleService: CustomTitleService) {
    this._title = 'my cabinet';

    this.titleService.setTitle(this._title);
  }

  get color(): string {
    return ThemeService.getCurrentTheme();
  }

  get title(): string {
    return this._title;
  }

  get nameCategory(): string {
    return CabinetService.getNameCategory();
  }

  ngOnInit() {
  }

}
