export interface Api {
    code: number,
    data: {
        result: {
            code: number,
            message: string,
            additional_info?: string,
        }
    },
    error: object,
    status: number,
    timestamp: {
        date_time: string,
        time_zone: string
    }
}
