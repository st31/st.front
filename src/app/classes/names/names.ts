export class Names {

    private _nameProject: string;

    constructor() {
        this._nameProject = 'Self-Testing';
    }

    get nameProject() {
        return this._nameProject;
    }
}
