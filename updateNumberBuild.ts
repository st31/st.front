var fs = require("fs");

let pathToPackageJson = './package.json';
let packageJson = require(pathToPackageJson);
packageJson['build'] = new Date().getTime();

fs.writeFile(pathToPackageJson, JSON.stringify(packageJson, null, 4), (err) => {
    if (err) {
        console.error(`Update number build - failed: ${err}`);
        return;
    };

    console.log("Number build - successfully update");
});